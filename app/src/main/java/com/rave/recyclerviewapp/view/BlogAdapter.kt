package com.rave.recyclerviewapp.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.recyclerviewapp.databinding.LayoutBlogListItemBinding
import com.rave.recyclerviewapp.model.entity.Blog

/**
 * Blog adapter.
 *
 * @constructor Create empty Blog adapter
 */
class BlogAdapter : RecyclerView.Adapter<BlogAdapter.BlogViewHolder>() {

    private val blogPostList: MutableList<Blog> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BlogViewHolder {
        return BlogViewHolder(parent)
    }

    override fun onBindViewHolder(holder: BlogViewHolder, position: Int) {
        holder.bindBlog(blogPostList[position])
    }

    override fun getItemCount(): Int = blogPostList.size

    /**
     * Load blogs.
     *
     * @param newBlogs
     */
    fun loadBlogs(newBlogs: List<Blog>) {
        val startPosition = this.blogPostList.size
        this.blogPostList.addAll(newBlogs)
        notifyItemRangeInserted(startPosition, newBlogs.size)
    }

    /**
     * Blog view holder.
     *
     * @property binding
     * @constructor Create empty Blog view holder
     */
    class BlogViewHolder(private val binding: LayoutBlogListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        /**
         * Bind blog.
         *
         * @param blogPost
         */
        fun bindBlog(blogPost: Blog) {
            binding.blogListName.text = blogPost.name
        }

        companion object {
            /**
             * Invoke.
             *
             * @param parent
             * @return
             */
            operator fun invoke(parent: ViewGroup): BlogViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val itemBinding = LayoutBlogListItemBinding.inflate(inflater, parent, false)
                return BlogViewHolder(itemBinding)
            }
        }
    }
}
