package com.rave.recyclerviewapp.view

import androidx.appcompat.app.AppCompatActivity
import com.rave.recyclerviewapp.R
import dagger.hilt.android.AndroidEntryPoint

/**
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)
