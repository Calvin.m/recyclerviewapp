package com.rave.recyclerviewapp.view

import com.rave.recyclerviewapp.model.entity.Blog

/**
 * Blog list state.
 *
 * @property isLoading
 * @property blogList
 * @constructor Create empty Blog list state
 */
data class BlogListState(
    val isLoading: Boolean = false,
    val blogList: List<Blog> = emptyList()
)
