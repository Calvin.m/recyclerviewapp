package com.rave.recyclerviewapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.recyclerviewapp.databinding.FragmentListBinding
import com.rave.recyclerviewapp.viewmodel.BlogViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * List fragment.
 *
 * @constructor Create empty List fragment
 */
@AndroidEntryPoint
class ListFragment : Fragment() {
    private val blogViewModel by viewModels<BlogViewModel>()
    private var _binding: FragmentListBinding? = null
    private val blogAdapter by lazy { BlogAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentListBinding.inflate(inflater, container, false).apply {
            _binding = this
            rvListing.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = this@ListFragment.blogAdapter
            }
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        blogViewModel.state.observe(viewLifecycleOwner) { state ->
            blogAdapter.loadBlogs(state.blogList)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
