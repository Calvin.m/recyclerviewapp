package com.rave.recyclerviewapp.model.entity

/**
 * Blog.
 *
 * @property name
 * @constructor Create empty Blog
 */
data class Blog(
    val name: String
)
