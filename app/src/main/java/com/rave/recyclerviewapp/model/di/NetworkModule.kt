package com.rave.recyclerviewapp.model.di

import com.rave.recyclerviewapp.model.remote.BlogService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Provides
    @Singleton
    fun providesService(): BlogService = BlogService()
}
