package com.rave.recyclerviewapp.model.remote

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.rave.recyclerviewapp.model.dto.BlogResponseDTO
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Blog service.
 *
 * @constructor Create empty Blog service
 */
@OptIn(ExperimentalSerializationApi::class)
interface BlogService {

    @GET("blog-posts")
    suspend fun getBlogs(@Query("hapikey") apiKey: String = "demo"): Response<BlogResponseDTO>

    companion object {
        private const val BASE_URL = "https://api.hubapi.com/content/api/v2/"

        /**
         * Invoke.
         *
         * @return
         */
        operator fun invoke(): BlogService {
            val json = Json {
                explicitNulls = false
                coerceInputValues = true
                ignoreUnknownKeys = true
            }
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(json.asConverterFactory("application/json".toMediaType()))
                .build()
                .create()
        }
    }
}
