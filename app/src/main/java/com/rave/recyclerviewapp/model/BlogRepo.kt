package com.rave.recyclerviewapp.model

import com.rave.recyclerviewapp.model.dto.BlogDTO
import com.rave.recyclerviewapp.model.entity.Blog
import com.rave.recyclerviewapp.model.remote.BlogService
import javax.inject.Inject

/**
 * Blog repo.
 *
 * @property blogService
 * @constructor Create empty Blog repo
 */
class BlogRepo @Inject constructor(
    private val blogService: BlogService
) {
    /**
     * Fetch all user account information and extracts all non null card infomation.
     *
     * @return
     */
    suspend fun getAllBlogs(): Result<List<Blog>> {
        return if (blogService.getBlogs().isSuccessful) {
            val blogsDTO = blogService.getBlogs()
            val blogs = blogsDTO.body()?.objects?.map { it.toMapBlog() } ?: emptyList()
            Result.success(blogs)
        } else {
            Result.failure(throw java.lang.IllegalArgumentException("Error"))
        }
    }

    private fun BlogDTO.toMapBlog(): Blog {
        return Blog(
            name = name
        )
    }
}
