package com.rave.recyclerviewapp.model.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class BlogResponseDTO(
    @SerialName("objects")
    val objects: List<BlogDTO>
)
