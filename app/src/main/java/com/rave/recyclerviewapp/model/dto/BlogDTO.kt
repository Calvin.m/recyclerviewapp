package com.rave.recyclerviewapp.model.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class BlogDTO(
    @SerialName("name")
    val name: String
)
