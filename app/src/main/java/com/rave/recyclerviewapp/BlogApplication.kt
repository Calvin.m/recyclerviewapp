package com.rave.recyclerviewapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Blog application.
 *
 * @constructor Create empty Blog application
 */
@HiltAndroidApp
class BlogApplication : Application()
