package com.rave.recyclerviewapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.recyclerviewapp.model.BlogRepo
import com.rave.recyclerviewapp.model.entity.Blog
import com.rave.recyclerviewapp.view.BlogListState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Blog view model.
 *
 * @property repo
 * @constructor Create empty Blog view model
 */
@HiltViewModel
class BlogViewModel @Inject constructor(
    private val repo: BlogRepo
) : ViewModel() {

    private val _state = MutableLiveData(BlogListState())
    val state: LiveData<BlogListState> get() = _state

    init {
        fetchBlogs()
    }

    private fun fetchBlogs() {
        viewModelScope.launch {
            _state.isLoading(true)
            viewModelScope.launch {
                repo.getAllBlogs()
                    .onSuccess { blogs -> _state.isSuccess(blogs) }
                    .onFailure { _state.isLoading(false) }
            }
        }
    }

    private fun MutableLiveData<BlogListState>.isLoading(isLoading: Boolean) {
        value = value?.copy(isLoading = isLoading)
    }

    private fun MutableLiveData<BlogListState>.isSuccess(blogs: List<Blog>) {
        value = value?.copy(isLoading = false, blogList = blogs)
    }
}
